<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Route::statamic('van-camping', 'van-camping.index', ['layout' => 'app-statamic']);

Route::statamic('van_camping/topics/{tag}', 'topics.show', ['layout' => 'app-statamic']);

Route::statamic('topics', 'topics.index', ['layout' => 'app-statamic']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
  return view('dashboard');
})->name('dashboard');
