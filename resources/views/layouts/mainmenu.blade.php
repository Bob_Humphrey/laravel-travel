@php
use Illuminate\Support\Str;
$path = request()->path();
$home = Str::of($path)->exactly('/') ? true : false;
$vanCamping = Str::of($path)->contains('van-camping') ? true : false;
// Log::info($path);
// Log::info('home=' . $home);
// Log::info('vanCamping=' . $vanCamping);
@endphp

<a href="{{ url('/van-camping') }}" class="{{ $vanCamping ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Van Camping
</a>


@guest
  {{-- <a href="{{ route('register') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Sign Up
  </a> --}}
  <a href="{{ route('login') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/cp/dashboard') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Admin
  </a>
  <a href="{{ url('/logout') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Logout
  </a>
@endauth
