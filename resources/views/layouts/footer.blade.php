@php
use Carbon\Carbon;
$year = Carbon::now()->format('Y');
@endphp

<footer class="bg-gray-400 uppercase font-inter_regular text-sm py-16 px-10 lg:px-20">

  <div class="flex w-full justify-center pb-16">
    <div class="w-16">
      <a href="https://bob-humphrey.com">
        <img src="/img/bh-logo.gif" alt="Bob Humphrey Web Development logo">
      </a>
    </div>
  </div>

  <div class="w-full text-gray-700 text-center mb-1">
    DOG SMILE FACTORY
  </div>
  <div class="w-full text-gray-700 text-center ">
    &copy; {{ $year }} Bob Humphrey - All rights reserved
  </div>

</footer>