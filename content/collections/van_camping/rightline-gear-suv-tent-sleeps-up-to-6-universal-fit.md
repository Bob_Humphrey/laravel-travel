---
title: 'Rightline Gear SUV Tent, Sleeps Up to 6, Universal Fit'
link: 'https://www.amazon.com/Rightline-Gear-110907-SUV-Tent/dp/B00NGJEN5K/ref=sr_1_2?dchild=1&keywords=Van+Tents+for+Camping&qid=1602869653&sr=8-2'
updated_by: 2
updated_at: 1602870807
topics:
  - tents
id: 5a9f20a5-8a98-4b70-833d-b2990f29d5a1
---
The Right line Gear SUV Tent lets you sleep off the ground in the comfort of your own vehicle. The tent connects to the back of any size SUV, minivan, wagon, or pick-up truck with cap. Large size no-see-um mesh windows and doors have storm covers that can be closed for privacy. The SUV Tent features (2) gear pockets, a lantern hanging hook, and glow-in-the-dark zipper pulls. The vehicle sleeve easily disconnects, allowing you to leave the tent behind as you go about the day's adventures.